package com.biom4st3r.moenchantments;

import java.util.Random;

import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;
import com.biom4st3r.moenchantments.api.events.LivingEntityDamageEvent;
import com.biom4st3r.moenchantments.api.events.OnBlockBreakAttemptEvent;
import com.biom4st3r.moenchantments.api.events.OnBowArrowCreationEvent;
import com.biom4st3r.moenchantments.interfaces.ProjectileEntityEnchantment;
import com.biom4st3r.moenchantments.logic.ChaosArrowLogic;
import com.biom4st3r.moenchantments.logic.EnderProtectionLogic;
import com.biom4st3r.moenchantments.logic.VeinMinerLogic;
import com.google.common.base.Predicate;

import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity.PickupPermission;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public final class EventHanlders {
    private EventHanlders(){}
    
    public static void init()
    {
        enderProtection();
        familiarity();
        veinMining();
        hoarding();
        OnBowArrowCreationEvent.EVENT.register((bow,arrowStack,arrowEntity,player,elapsed,pullProg)->
        {
            ((ProjectileEntityEnchantment)arrowEntity).setAccuracyEnch(EnchantmentHelper.getLevel(EnchantmentRegistry.BOW_ACCURACY, bow));
            ((ProjectileEntityEnchantment)arrowEntity).setInAccuracyEnch(EnchantmentHelper.getLevel(EnchantmentRegistry.BOW_ACCURACY_CURSE, bow));
            if(ChaosArrowLogic.makePotionArrow(player, arrowEntity, player.getRandom()))
            {
                arrowEntity.pickupType = PickupPermission.CREATIVE_ONLY;
            }
        });
    }

    public static void hoarding() {
        LivingEntityDamageEvent.EVENT.register((damageSource,damage,entity,ci)->
        {
            if(damageSource.getAttacker() != null)
            {
                LivingEntity attacker = (LivingEntity) damageSource.getAttacker();
                int hoardingLvl = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.HOARDING, attacker);
                if(hoardingLvl > 0 && !(entity instanceof PlayerEntity) && entity instanceof Monster)
                {
                    Random random = entity.getEntityWorld().getRandom();
                    World world = entity.getEntityWorld();
                    for(int i = 0; i < 10; i++);
                    {
                        int x = entity.getBlockPos().getX() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
                        int y = entity.getBlockPos().getY() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
                        int z = entity.getBlockPos().getZ() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
                        BlockPos spawnPos = new BlockPos(x, y, z);
                        if(checkBox(world, new Box(spawnPos,spawnPos.up(3)), (blockstate)-> blockstate.isAir()))
                        {
                            world.spawnEntity(entity.getType().create(world));
                        }
                    }
                    
                }
            }
        });
    }
    public static void familiarity() {
        LivingEntityDamageEvent.EVENT.register((damageSource,damage,entity,ci)->
        {
            if ((entity) instanceof TameableEntity && damageSource.getAttacker() instanceof PlayerEntity) 
            {
                PlayerEntity attacker = (PlayerEntity) damageSource.getAttacker();
                TameableEntity defender = (TameableEntity) entity;
                if (EnchantmentSkeleton.hasEnchant(EnchantmentRegistry.TAMEDPROTECTION, attacker.getMainHandStack())
                        && defender.getOwnerUuid() != ModInit.uuidZero) 
                        {
                    if (ModInit.config.TameProtectsOnlyYourAnimals) 
                    {
                        if (defender.isOwner(attacker)) {
                            ci.setReturnValue(false);
                        }
                    } 
                    else 
                    {
                        ci.setReturnValue(false);
                    }
                }
            }
        });
    }
    public static void enderProtection() {
        LivingEntityDamageEvent.EVENT.register((damageSource,damage,entity,ci)->
        {
            int enderProLvl = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.ENDERPROTECTION, (LivingEntity)entity);
            if (enderProLvl > 0) {
                if (EnderProtectionLogic.doLogic(damageSource, enderProLvl, (LivingEntity)entity)) { 
                    ci.setReturnValue(false);
                }
            }
        });
    }
    public static void veinMining() {
        OnBlockBreakAttemptEvent.EVENT.register((manager,pos,reason)->
        {
            if(reason.isSuccessfulAndEffective())
            {
                PlayerEntity pe = manager.player;
                World world = pe.getEntityWorld();
                VeinMinerLogic.tryVeinMining(world, pos, world.getBlockState(pos), pe);
            }
            return ActionResult.PASS;
        });
    }
    public static boolean checkBox(World world, Box box, Predicate<BlockState> condition)
    {
        boolean result = true;
        for(int x = (int) box.minX; x < box.maxX; x++)
        {
            for(int y = (int) box.minY; y < box.maxY; y++)
            {
                for(int z = (int) box.minZ; z < box.maxZ; z++)
                {
                    result &= condition.apply(world.getBlockState(new BlockPos(x, y, z)));
                }
            }
        }
        return result;
    }

}