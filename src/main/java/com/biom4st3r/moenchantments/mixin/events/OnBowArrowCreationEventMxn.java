package com.biom4st3r.moenchantments.mixin.events;

import com.biom4st3r.moenchantments.api.events.OnBowArrowCreationEvent;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.PersistentProjectileEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(BowItem.class)
public abstract class OnBowArrowCreationEventMxn
{

    @Inject(
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/entity/projectile/PersistentProjectileEntity.setProperties(Lnet/minecraft/entity/Entity;FFFFF)V"),
        method="onStoppedUsing",
        locals = LocalCapture.CAPTURE_FAILHARD)
    public void addEnchantmentsToArrowEntity(ItemStack bow, World world, LivingEntity user, int remainingUseTicks, CallbackInfo ci,PlayerEntity player, boolean cretiveOrInfinity, ItemStack arrowStack, int elapsedUseTime, float pullProgress, boolean creativeOrInfinityAndArrowStackIsArrow, ArrowItem arrow, PersistentProjectileEntity arrowEntity)
    {
        OnBowArrowCreationEvent.EVENT.invoker().onCreation(bow, arrowStack,arrowEntity, player, elapsedUseTime, pullProgress);
        // ((ProjectileEntityEnchantment)arrowEntity).setAccuracyEnch(EnchantmentHelper.getLevel(EnchantmentRegistry.BOW_ACCURACY, bow));
        // ((ProjectileEntityEnchantment)arrowEntity).setInAccuracyEnch(EnchantmentHelper.getLevel(EnchantmentRegistry.BOW_ACCURACY_CURSE, bow));
    }

}