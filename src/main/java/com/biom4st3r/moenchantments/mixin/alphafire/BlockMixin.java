package com.biom4st3r.moenchantments.mixin.alphafire;

import java.util.List;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.logic.AlphafireLogic;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;

@Mixin(Block.class)
public class BlockMixin
{
    // @Inject(at = @At("HEAD"),method = "getDroppedStacks", cancellable = true)
    // private void doAlphaFire(LootContext.Builder lootBuilder, CallbackInfoReturnable<List<ItemStack>> ci)
    // {
    //     // ShulkerBoxBlock
    //     AlphafireLogic.doLogic((AbstractBlockState)(Object)this,  lootBuilder, ci);
    // }
    @Inject(
        at = @At("RETURN"), 
        method = "getDroppedStacks(Lnet/minecraft/block/BlockState;Lnet/minecraft/server/world/ServerWorld;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/entity/BlockEntity;Lnet/minecraft/entity/Entity;Lnet/minecraft/item/ItemStack;)Ljava/util/List;",
        cancellable = false
        )
        private static void biom4st3r_moenchantments_doAlphaFire(
            BlockState state, ServerWorld world, BlockPos pos, 
            BlockEntity blockEntity, Entity entity, ItemStack tool, CallbackInfoReturnable<List<ItemStack>> ci)
        {
            if(EnchantmentHelper.getLevel(EnchantmentRegistry.AUTOSMELT, tool) > 0)
            {
                AlphafireLogic.attemptSmeltStacks(world, entity, ci.getReturnValue());
            }
        }
}