package com.biom4st3r.moenchantments;

import com.biom4st3r.moenchantments.api.MoEnchantBuilder;
import com.biom4st3r.moenchantments.api.EnchantmentSkeleton;

import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.HoeItem;
import net.minecraft.item.Items;
import net.minecraft.item.MiningToolItem;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.SwordItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class EnchantmentRegistry {
    public static final String MODID = ModInit.MODID;

    public static final EnchantmentSkeleton TREEFELLER = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .maxlevel(ModInit.config.TreeFellerMaxBreakByLvl.length)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof AxeItem;})
        .minpower((level)->{return level*5;})
        .enabled(ModInit.config.EnableTreeFeller)
        .build("treefeller");
    public static final EnchantmentSkeleton VEINMINER = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .maxlevel(ModInit.config.VeinMinerMaxBreakByLvl.length)
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof PickaxeItem;})
        .minpower((level)->{return level*5;})
        .enabled(ModInit.config.EnableVeinMiner)
        .build("veinminer");
    public static final EnchantmentSkeleton AUTOSMELT = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .addExclusive(Enchantments.SILK_TOUCH)
        .enabled(ModInit.config.EnableAutoSmelt)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof MiningToolItem;})
        .addExclusive(Enchantments.SILK_TOUCH)
        .minpower((level)->{return level * 8;})
        .build("autosmelt");
    public static final EnchantmentSkeleton TAMEDPROTECTION = new MoEnchantBuilder(Rarity.COMMON, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof SwordItem || itemStack.getItem() instanceof AxeItem;})
        .treasure(true)
        .enabled(ModInit.config.EnableTamedProtection)
        .build("tamedprotection");
    public static final EnchantmentSkeleton ENDERPROTECTION = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.ARMOR, MoEnchantBuilder.ARMOR_SLOTS)
        .maxlevel(3)
        .minpower((level)->{return level*10;})
        .treasure(true)
        .curse(true)
        .enabled(ModInit.config.EnableEnderProtection)
        .build("curseofender");
    public static final EnchantmentSkeleton HAVESTER = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .minpower((level)->{return level*4;})
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof HoeItem;})
        .enabled(false)
        .build("harvester");
    //Gitlab @Mr Cloud
    public static final EnchantmentSkeleton SOULBOUND = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, MoEnchantBuilder.ALL_SLOTS)
        .enabled(ModInit.config.EnableSoulBound)
        .maxlevel(ModInit.config.UseStandardSoulboundMechanics ? 1 : ModInit.config.MaxSoulBoundLevel)
        .minpower((level)->{return 20+level;})
        .isAcceptible((itemstack)->{return true;})
        .build("soulbound");
    public static final EnchantmentSkeleton POTIONRETENTION = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .minpower((level)->{return 7*level;})
        .maxlevel(ModInit.config.PotionRetentionMaxLevel)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof SwordItem || itemstack.getItem() instanceof AxeItem;})
        .enabled(ModInit.config.EnablePotionRetention)
        .build("potionretension");
    // someone responding to Jeb_ on reddit.
    public static final EnchantmentSkeleton BOW_ACCURACY = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
        .maxlevel(2)
        .minpower((level)->{return (level-1)*10;})
        .maxpower((level)->{return (level-1)*10+5;})
        .enabled(ModInit.config.EnableAccuracy && !Plugin.extraBowsFound)
        .build("bowaccuracy");
    public static final EnchantmentSkeleton BOW_ACCURACY_CURSE = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
        .maxlevel(1)
        .curse(true)
        .addExclusive(BOW_ACCURACY)
        .enabled(ModInit.config.EnableInAccuracy && !Plugin.extraBowsFound)
        .build("bowinaccuracy");
    // Gitlab @cryum
    public static final EnchantmentSkeleton ARROW_CHAOS = new MoEnchantBuilder(Rarity.VERY_RARE,EnchantmentTarget.BOW,MoEnchantBuilder.HAND_SLOTS)
        .curse(true)
        .minpower((level)->{return 30;})
        .enabled(ModInit.config.EnableArrowChaos && !Plugin.extraBowsFound)
        .build("arrow_chaos");
    public static final EnchantmentSkeleton BUILDERS_WAND = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, MoEnchantBuilder.HAND_SLOTS)
        .treasure(true)
        .minpower((level)->{return 10;})
        .enabled(false)
        .isAcceptible((itemstack)->{return itemstack.getItem() == Items.STICK;})
        .build("builders_wand");
        //https://www.reddit.com/r/minecraftsuggestions/comments/dx8zom/shield_enchantments/
        public static final EnchantmentSkeleton SHIELD_REFLECT = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, EquipmentSlot.OFFHAND)
        .maxlevel(3)
        .treasure(true)
        .enabled(false)
        .build("reflectarrow");
    public static final EnchantmentSkeleton TRAINING_WEAPON = new MoEnchantBuilder(Rarity.RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .maxlevel(1)
        .treasure(true)
        .enabled(false)//ModInit.config.EnableTrainingWeapon)
        .build("training_weapon"); // Provide more xp from kill. but reduce damage;
    public static final EnchantmentSkeleton VAMPIRISM = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .treasure(true)
        .minpower((level)-> {return 10;})
        .maxpower((level)->{return 15;})
        .enabled(false)//ModInit.config.EnableVampirism)
        .build("vampirism");
    public static final EnchantmentSkeleton HOARDING = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .curse(true)
        .minpower((level)->{return 20;})
        .maxpower((level)->{return 30;})
        .enabled(false)//ModInit.config.EnableHording)
        .build("hording");
    public static final EnchantmentSkeleton SHIELD_BASH = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.WEARABLE, MoEnchantBuilder.HAND_SLOTS)
        .treasure(true)
        .minpower((level)->{return 10;})
        .enabled(false)
        .addExclusive(SHIELD_REFLECT)
        .build("shield_bash");
    public static final EnchantmentSkeleton TELEPORTING_ARROW = new MoEnchantBuilder(Rarity.VERY_RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
        .treasure(true)
        .minpower((l)->30)
        .enabled(false)
        .addExclusive(Enchantments.MULTISHOT).addExclusive(Enchantments.INFINITY).addExclusive(ARROW_CHAOS)
        .build("teleport_arrow");
    public static final EnchantmentSkeleton ARROW_DISTANCE = new MoEnchantBuilder(Rarity.UNCOMMON, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
        .minpower((level)->8)
        .enabled(false)
        .build("oomph");
    public static void init()
    {
        for(EnchantmentSkeleton e : new EnchantmentSkeleton[] {
            TREEFELLER,VEINMINER,AUTOSMELT,
            TAMEDPROTECTION,ENDERPROTECTION,POTIONRETENTION,
            HAVESTER,SOULBOUND,BOW_ACCURACY,
            BOW_ACCURACY_CURSE,ARROW_CHAOS,})
        {
            if(e.enabled())
                Registry.register(Registry.ENCHANTMENT, new Identifier(MODID, e.regName()), e);
        }
    }
}