package com.biom4st3r.moenchantments.logic;

import java.util.List;
import java.util.Optional;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.SmeltingRecipe;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;

/**
 * AlphafireLogic
 */
public class AlphafireLogic {

    public static void attemptSmeltStacks(ServerWorld world,Entity entity, List<ItemStack> dropList)
    {
        RecipeManager rm = world.getRecipeManager(); 
        Inventory basicInv = new SimpleInventory(1);
        
        ItemStack itemToBeChecked = ItemStack.EMPTY;
        Optional<SmeltingRecipe> smeltingResult;
        
        for(int stacksIndex = 0; stacksIndex < dropList.size(); stacksIndex++)
        {
            itemToBeChecked = dropList.get(stacksIndex);
            // basicInv = new SimpleInventory(itemToBeChecked);
            basicInv.setStack(0, itemToBeChecked);
            smeltingResult = rm.getFirstMatch(RecipeType.SMELTING, basicInv, entity.world);
            if(smeltingResult.isPresent() && !(ModInit.autoSmelt_blacklist.contains(itemToBeChecked.getItem())))
            {
                dropList.set(stacksIndex, new ItemStack(smeltingResult.get().getOutput().getItem(),itemToBeChecked.getCount()));
                ((PlayerExperienceStore)(Object)entity).addExp(smeltingResult.get().getExperience() * itemToBeChecked.getCount());
                if(!ModInit.lockAutoSmeltSound)
                {
                    ModInit.lockAutoSmeltSound = true;
                    world.playSound((PlayerEntity)null, entity.getBlockPos(), SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.1f, 0.1f);
                }
            }
        }
    }

}