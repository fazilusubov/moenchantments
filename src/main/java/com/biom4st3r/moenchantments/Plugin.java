package com.biom4st3r.moenchantments;

import java.util.List;
import java.util.Set;

import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

import net.fabricmc.loader.api.FabricLoader;

/**
 * Plugin
 */
public class Plugin implements IMixinConfigPlugin {
    BioLogger logger = ModInit.logger;

    public static boolean extraBowsFound = false;

    @Override
    public void onLoad(String mixinPackage) {

    }

    @Override
    public String getRefMapperConfig() {
        return null;
    }

    @Override
    public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
        if(mixinClassName.contains("com.biom4st3r.moenchantments.mixin.bowaccuracy") || mixinClassName.contains("com.biom4st3r.moenchantments.mixin.arrowchaos"))
        {
            if(FabricLoader.getInstance().isModLoaded("extrabows"))
            {
                extraBowsFound = true;
                logger.error("MoEnchantments bow enchantments are incompatible with Extra Bows. Their functionality has been disabled. Please disable them in your moenchantconfig.json");
                return false;
            }
        }
        return true;
    }

    @Override
    public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {

    }

    @Override
    public List<String> getMixins() {
        return null;
    }

    @Override
    public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    @Override
    public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {

    }

    
}